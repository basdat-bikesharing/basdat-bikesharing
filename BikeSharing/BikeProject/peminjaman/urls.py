from django.urls import path, include
from . import views

urlpatterns = [
    path('create_peminjaman/', views.create_peminjaman, name='create_peminjaman'),   
    path('list_peminjaman/',views.list_peminjaman, name='list_peminjaman'),
]
