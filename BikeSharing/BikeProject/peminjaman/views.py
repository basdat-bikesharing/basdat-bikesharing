from django.shortcuts import render

# Create your views here.
def create_peminjaman(request):
    return render(request, 'create_peminjaman.html')

def list_peminjaman(request):
    return render(request, 'list_peminjaman.html')
