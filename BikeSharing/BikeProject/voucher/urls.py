from django.urls import path, include
from . import views

urlpatterns = [
    path('create_voucher/', views.create_voucher, name='create_voucher'),   
    path('list_voucher/',views.list_voucher, name='list_voucher'),
    path('update_voucher/', views.update_voucher, name='update_voucher')
]
