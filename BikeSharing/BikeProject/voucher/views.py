from django.shortcuts import render

# Create your views here.
def create_voucher(request):
    return render(request, 'create_voucher.html')

def list_voucher(request):
    return render(request, 'list_voucher.html')

def update_voucher(request):
	return render(request, 'update_voucher.html')