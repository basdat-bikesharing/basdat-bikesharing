from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf import settings
from . import views
from BikeApp import views as appViews
app_name = 'BikeApp'
urlpatterns = [
    path('',views.home),
    path('about/',views.about),
    path('login/', views.login),
    path('daftar/', views.daftar),
    path('topup/', views.topup),
    path('riwayat/', views.riwayat),
    # path('logout/', views.login, name='logout'),
    # path('auth/', include('social_django.urls', namespace='social'))
    # url(r'^auth/', include('social_django.urls', namespace='social')),
    
]