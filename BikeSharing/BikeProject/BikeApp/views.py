from django.shortcuts import render

def home(request):
    return render(request,'base.html')

def about(request):
    return render(request, 'about.html')

def login(request):
    return render(request, 'Login.html')

def logout(request):
    return render(request, 'base.html')

def daftar(request):
    return render(request, 'daftar.html')

def topup(request):
    return render(request, 'topUp.html')

def riwayat(request):
    return render(request, 'riwayat.html')
    