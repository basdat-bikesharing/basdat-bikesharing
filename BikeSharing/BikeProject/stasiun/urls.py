from django.urls import path
from . import views

urlpatterns = [
    path('create_stasiun/', views.create_stasiun, name='create_stasiun'),   
    path('list_stasiun/',views.list_stasiun, name='list_stasiun'),
    path('update_stasiun/', views.update_stasiun, name='update_stasiun')
]
