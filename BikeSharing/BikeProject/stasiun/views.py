from django.shortcuts import render

# Create your views here
def create_stasiun(request):
    return render(request, 'create_stasiun.html')

def list_stasiun(request):
    return render(request, 'list_stasiun.html')

def update_stasiun(request):
	return render(request, 'update_stasiun.html')
