from django.urls import path
from . import views

urlpatterns = [
    path('create_sepeda/', views.create_sepeda, name='create_sepeda'),   
    path('list_sepeda/',views.list_sepeda, name='list_sepeda'),
    path('update_sepeda/', views.update_sepeda, name='update_sepeda')
]
