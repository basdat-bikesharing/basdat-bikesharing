from django.shortcuts import render


# Create your views here.
def create_sepeda(request):
    return render(request, 'create_sepeda.html')

def list_sepeda(request):
    return render(request, 'list_sepeda.html')

def update_sepeda(request):
	return render(request, 'update_sepeda.html')
